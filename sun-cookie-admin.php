<?php
// create custom plugin settings menu
add_action('admin_menu', 'sun_cookie_plugin_create_menu');

function sun_cookie_plugin_create_menu() {

	//create new top-level menu
	add_plugins_page('SUN Cookie options', 'SUN Cookie options', 'administrator', __FILE__, 'sun_cookie_plugin_settings_page' , plugins_url('/images/icon.png', __FILE__) );

	//call register settings function
	add_action( 'admin_init', 'register_sun_cookie_plugin_settings' );
}


function register_sun_cookie_plugin_settings() {
	//register our settings
	register_setting( 'sun-cookie-plugin-settings-group', 'cookie_alignment' );
	register_setting( 'sun-cookie-plugin-settings-group', 'cookie_offset' );
	register_setting( 'sun-cookie-plugin-settings-group', 'cookie_bg_color' );
}

function sun_cookie_plugin_settings_page() {
?>

<div class="wrap">
<h2>SUN Cookie settings</h2>

<form method="post" action="options.php">
    <?php settings_fields( 'sun-cookie-plugin-settings-group' ); ?>
    <?php do_settings_sections( 'sun-cookie-plugin-settings-group' ); ?>
    <table class="form-table">
        <tr valign="top">
        <th scope="row">Alignment</th>
        <td><input type="text" name="cookie_alignment" value="<?php echo esc_attr( get_option('cookie_alignment') ); ?>" /> right / left</td>
        </tr>
         
        <tr valign="top">
        <th scope="row">Offset</th>
        <td><input type="text" name="cookie_offset" value="<?php echo esc_attr( get_option('cookie_offset') ); ?>" /> px</td>
        </tr>
        
        <tr valign="top">
        <th scope="row">Background color</th>
        <td><input type="text" name="cookie_bg_color" value="<?php echo esc_attr( get_option('cookie_bg_color') ); ?>" /> red / blue or #000000</td>
        </tr>
    </table>
    
    <?php submit_button(); ?>

</form>
</div>
<?php } ?>