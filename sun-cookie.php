<?php
/*
Plugin Name: SUN Cookie Policy
Plugin URI: https://bitbucket.org/startupnow/sun-cookie
Description: To help compliance with the UK interpretation of the EU regulations regarding usage of website cookies.
Author: Start Up Now
Version: 2.0.3
Author URI: http://startupnow.co.uk/
*/

include( plugin_dir_path( __FILE__ ) . '/sun-cookie-admin.php');

if (!function_exists('is_admin')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}
// Pre-2.6 compatibility
if ( ! defined( 'WP_CONTENT_URL' ) )
      define( 'WP_CONTENT_URL', get_option( 'siteurl' ) . '/wp-content' );
if ( ! defined( 'WP_CONTENT_DIR' ) )
      define( 'WP_CONTENT_DIR', ABSPATH . 'wp-content' );
	  
define( 'SUN-cookie_DIR', WP_PLUGIN_DIR . '/SUN-cookie' );
define( 'SUN-cookie_URL', WP_PLUGIN_URL . '/SUN-cookie' );


function load_js_file()
{
	wp_enqueue_script('jquery');
	wp_enqueue_script('jquery', plugins_url('js/jquery.min.js',__FILE__) );
	wp_enqueue_script('cookie-js', plugins_url('js/cookie-set.js',__FILE__) );
	wp_enqueue_script('control-js', plugins_url('js/control.js',__FILE__) );
}
add_action('wp_footer', 'load_js_file');

function load_style_file() {
	wp_register_style( 'namespace', plugins_url ( 'style/cookie.css', __FILE__ ) );
	wp_enqueue_style('namespace');	
}
add_action('wp_footer', 'load_style_file');

function display_cookie_bar() {
echo '<style type="text/css">#popupDiv {'. esc_attr( get_option('cookie_alignment') ) .':'. esc_attr( get_option('cookie_offset') ) .'px !important;background:'. esc_attr( get_option('cookie_bg_color') ) .' !important;}</style>';
echo '<div id="popupDiv" style="display:none;background-repeat:no-repeat;);">';
echo '<img src="' . plugins_url( 'images/Animated-Cookie-1.gif' , __FILE__ ) . '" > ';
echo '<div>';
echo '<p>Hello.<br />We use cookies on this website to help us and our partners improve your browsing experience.<br />';
echo '<a href="JavaScript:void(0)" id="close" class="closebuttoncookie">Close</a><a href="' . plugins_url( 'cookie-policy.php' , __FILE__ ) . '" target="_blank">Manage Cookies</a></p>';
echo '</div>';
echo '</div>';
}
add_action('wp_head', 'display_cookie_bar');

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = PucFactory::buildUpdateChecker(
    'http://aoatc.org/sun_plugins.json',
    __FILE__
);
?>
